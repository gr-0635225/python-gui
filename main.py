#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QApplication

from score import Score

# Required for GUI
app = QApplication(sys.argv)

# Create scoring window object & initialise UI
score = Score()
score.initialiseUI()

# Set values
score.setTeam1Name('AUT University')
score.setTeam2Name('University of Auckland')
score.setTeam1Score(999)
score.setTeam2Score(50)

# Exit once window closed
sys.exit(app.exec_())
