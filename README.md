# Python GUI

GUI component for ESE 2016's group assignment.

## Prerequisites

*   [Python][url-python] 3
*   Python bindings for [QT][url-qt] 5

Install with Raspbian's package manager `sudo apt-get install python3 python3-pyqt5`

### Usage

Run with `python3 main.py`

[url-qt]:https://www.qt.io/
[url-python]:https://www.python.org/
