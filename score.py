#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QLabel, QLineEdit,
QTextEdit, QGridLayout, QApplication, QLCDNumber, QFrame)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont

class Score(QWidget):

    def __init__(self):
        super().__init__()

        # Class members
        self.font = QFont("SansSerif", 40, QFont.Bold)
        self.team1Name = QLabel()
        self.team2Name = QLabel()
        self.team1Score = QLCDNumber()
        self.team2Score = QLCDNumber()
        self.grid = QGridLayout()

        # Set initial values
        self.team1Name.setText('Team 1')
        self.team2Name.setText('Team 2')
        self.team1Score.display(0)
        self.team2Score.display(0)

    def initialiseUI(self):
        # Set font for team names
        self.team1Name.setFont(self.font)
        self.team2Name.setFont(self.font)

        # Unused (at the moment)
        #self.team1Name.setStyleSheet("QWidget { color: yellow; }")
        #self.team1Score.setDigitCount(3)

        # Remove border from LCD widgets
        self.team1Score.setFrameStyle(QFrame.NoFrame)
        self.team2Score.setFrameStyle(QFrame.NoFrame)

        # Initialise grid
        self.grid.setSpacing(10)

        # Add team name widgets
        self.grid.addWidget(self.team1Name, 1, 1, 2, 2)
        self.grid.addWidget(self.team2Name, 2, 1, 2, 2)

        # Add score widgets
        self.grid.addWidget(self.team1Score, 1, 3, 2, 2)
        self.grid.addWidget(self.team2Score, 2, 3, 2, 2)

        # Set layout (grid)
        self.setLayout(self.grid)

        # Set window title & fullscreen
        self.setWindowTitle('Score')
        self.showFullScreen()

        # Used for debugging. Ignore me
        #self.setGeometry(300, 300, 1024, 768)
        #self.show()

    def setTeam1Name(self, name):
        self.team1Name.setText(name)

    def setTeam2Name(self, name):
        self.team2Name.setText(name)

    def setTeam1Score(self, score):
        self.team1Score.display(score)

    def setTeam2Score(self, score):
        self.team2Score.display(score)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    score = Score()
    score.initialiseUI()
    sys.exit(app.exec_())
